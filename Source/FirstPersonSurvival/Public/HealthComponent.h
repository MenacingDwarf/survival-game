// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

class AFirstPersonSurvivalCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FIRSTPERSONSURVIVAL_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void DeathEvent();

	UFUNCTION()
	void ReceivingDamageEffects(float Damage);

	UFUNCTION()
	void ReceivingHealEffects(float HealSize);

	UFUNCTION()
	void DeathEffects();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// HP parameters
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bIsDead;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MaxHP;

	UPROPERTY(BlueprintReadWrite)
	float CurrentHP;

	// HP Changing methods
	UFUNCTION(BlueprintCallable)
	void ReceiveDamage(float Damage);

	UFUNCTION(BlueprintCallable)
	void ReceiveHeal(float HealSize);

	// Hunger parameters
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MaxHunger;

	UPROPERTY(BlueprintReadWrite)
	float CurrentHunger;

	UPROPERTY(EditDefaultsOnly)
	float PeriodicalHungerLosingAmount;

	UPROPERTY(EditDefaultsOnly)
	float PeriodicalHungerLosingFrequency;

	AFirstPersonSurvivalCharacter* PlayerRef;

private:
	FTimerHandle PeriodicalHungerLosingTimer;

	UFUNCTION()
	void PeriodicalHungerLosingEvent();

	FTimerHandle HealthWidgetInitializingTimer;

	UFUNCTION()
	void UpdateHealthWidget();

};
