// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MasterItem.h"
#include "PlacableItem.generated.h"

UCLASS()
class FIRSTPERSONSURVIVAL_API APlacableItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlacableItem();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AMasterItem> ItemClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* StaticMesh;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetItemClass(TSubclassOf<class AMasterItem> NewItemClass);

};
