// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

class AFirstPersonSurvivalCharacter;
class AMasterItem;

USTRUCT(BlueprintType)
struct FSlotStructure {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AMasterItem> ItemClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Amount = 0;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FIRSTPERSONSURVIVAL_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float SlotsAmount;

	UPROPERTY(BlueprintReadOnly)
	TArray<FSlotStructure> Slots;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	AFirstPersonSurvivalCharacter* PlayerRef;

	bool SearchEmptySlot(int& SlotIndex);

	bool SearchSlotForStack(TSubclassOf<AMasterItem> ItemClass, int Amount, int& SlotIndex);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	bool AddItem(TSubclassOf<AMasterItem> ItemClass, int Amount = 1);

	UFUNCTION(BlueprintCallable)
	bool UseItemAtIndex(float ItemIndex);

	UFUNCTION(BlueprintCallable)
	bool RemoveItemAtIndex(float ItemIndex, int Amount = 1);
};
