// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MasterItem.h"
#include "WallLightItem.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSONSURVIVAL_API AWallLightItem : public AMasterItem
{
	GENERATED_BODY()

public:
	AWallLightItem();
	
};
