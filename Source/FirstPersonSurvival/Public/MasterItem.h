// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Texture2D.h"
#include "MasterItem.generated.h"

class AFirstPersonSurvivalCharacter;

UENUM(BlueprintType)
enum EItemType
{
	Food       UMETA(DisplayName = "Food"),
	Thing      UMETA(DisplayName = "Thing"),
	Building   UMETA(DisplayName = "Building"),
	Weapon     UMETA(DisplayName = "Weapon"),
};

USTRUCT(BlueprintType)
struct FItemSignature{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bPlacable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bUsable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bStackable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EItemType> Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> PossibleSurfacesToPlace;
};

UCLASS()
class FIRSTPERSONSURVIVAL_API AMasterItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMasterItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UStaticMesh* StatisMesh;

	UFUNCTION()
	virtual void ItemSkill(AFirstPersonSurvivalCharacter* PlayerRef);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FItemSignature ItemStructure;

	UFUNCTION()
	virtual bool UseItem(AFirstPersonSurvivalCharacter* PlayerRef);
};
