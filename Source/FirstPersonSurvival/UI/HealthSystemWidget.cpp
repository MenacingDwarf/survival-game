// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthSystemWidget.h"
#include "../FirstPersonSurvivalCharacter.h"
#include "../Public/HealthComponent.h"

UHealthSystemWidget::UHealthSystemWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UHealthSystemWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UHealthSystemWidget::UpdateHP(int CurrentHP, int MaxHP)
{
	if (TXTCurrentHP && TXTMaxHP)
	{
		
		TXTCurrentHP->SetText(FText::FromString(FString::FromInt(CurrentHP)));
		TXTMaxHP->SetText(FText::FromString(FString::FromInt(MaxHP)));
		HPProgressBar->SetPercent(1.f * CurrentHP / MaxHP);
	}
}

void UHealthSystemWidget::UpdateHunger(int CurrentHunger, int MaxHunger)
{
	if (TXTCurrentHunger && TXTMaxHunger)
	{
		
		TXTCurrentHunger->SetText(FText::FromString(FString::FromInt(CurrentHunger)));
		TXTMaxHunger->SetText(FText::FromString(FString::FromInt(MaxHunger)));
		HungerProgressBar->SetPercent(1.f * CurrentHunger / MaxHunger);
	}
}
