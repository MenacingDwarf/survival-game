// Fill out your copyright notice in the Description page of Project Settings.


#include "MainHUD.h"
#include "Kismet/KismetMathLibrary.h"

AMainHUD::AMainHUD()
{

}

void AMainHUD::DrawHUD()
{
	Super::DrawHUD();
}

void AMainHUD::BeginPlay()
{
	Super::BeginPlay();

	if (HealthSystemWidgetClass)
	{
		HealthSystemWidget = CreateWidget<UHealthSystemWidget>(GetWorld(), HealthSystemWidgetClass);

		if (HealthSystemWidget)
		{
			HealthSystemWidget->AddToViewport();
		}
	}
}

void AMainHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AMainHUD::UpdateHealthSystem(float CurrentHP, float MaxHP, float CurrentHunger, float MaxHunger)
{
	if (HealthSystemWidget)
	{
		HealthSystemWidget->UpdateHP(UKismetMathLibrary::FCeil(CurrentHP), UKismetMathLibrary::FCeil(MaxHP));
		HealthSystemWidget->UpdateHunger(UKismetMathLibrary::FCeil(CurrentHunger), UKismetMathLibrary::FCeil(MaxHunger));
	}
}
