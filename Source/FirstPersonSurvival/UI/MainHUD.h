// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "Components/WidgetComponent.h"
#include "HealthSystemWidget.h"

#include "MainHUD.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSONSURVIVAL_API AMainHUD : public AHUD
{
	GENERATED_BODY()
public:
	AMainHUD();

	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	void UpdateHealthSystem(float CurrentHP, float MaxHP, float CurrentHunger, float MaxHunger);

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UUserWidget> HealthSystemWidgetClass;

private:
	UHealthSystemWidget* HealthSystemWidget;
};
