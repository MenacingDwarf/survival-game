// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FirstPersonSurvivalCharacter.h"
#include "FirstPersonSurvivalProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "MotionControllerComponent.h"
#include "Public/InventoryComponent.h"
#include "Public/HealthComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "Public/PlacableItem.h"
#include "UI/MainHUD.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFirstPersonSurvivalCharacter

AFirstPersonSurvivalCharacter::AFirstPersonSurvivalCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	bItemPlaceMode = false;
	ItemRotation = { 0, 0, 0 };
	ToPlaceItemClass = nullptr;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory"));
	Inventory->SlotsAmount = 20;

	HealthSystem = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthSystem"));
	HealthSystem->MaxHP = 100.f;
	HealthSystem->PlayerRef = this;
}

void AFirstPersonSurvivalCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	MainHUDRef = Cast<AMainHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
}

void AFirstPersonSurvivalCharacter::Tick(float DeltaSeconds)
{
	if (bItemPlaceMode) {
		if (IsValid(ToPlaceItemRef)) {
			ToPlaceItemRef->Destroy();
		}
		PlaceItem();
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFirstPersonSurvivalCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("ActionButton", IE_Pressed, this, &AFirstPersonSurvivalCharacter::OnActionButton);
	PlayerInputComponent->BindAction("RotateItemLeft", IE_Pressed, this, &AFirstPersonSurvivalCharacter::OnRotateItemLeft);
	PlayerInputComponent->BindAction("RotateItemRight", IE_Pressed, this, &AFirstPersonSurvivalCharacter::OnRotateItemRight);


	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFirstPersonSurvivalCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFirstPersonSurvivalCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFirstPersonSurvivalCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFirstPersonSurvivalCharacter::LookUpAtRate);
}

void AFirstPersonSurvivalCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFirstPersonSurvivalCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void AFirstPersonSurvivalCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFirstPersonSurvivalCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFirstPersonSurvivalCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFirstPersonSurvivalCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFirstPersonSurvivalCharacter::OnActionButton()
{
	if (bItemPlaceMode) {
		ToPlaceItemRef = nullptr;
	}
	bItemPlaceMode = !bItemPlaceMode;
}

void AFirstPersonSurvivalCharacter::PlaceItem()
{
	FHitResult OutHit;
	FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
	FVector End = ((ForwardVector * 1000.f) + Start);
	FCollisionQueryParams CollisionParams;
	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams)) {

		ToPlaceItemRef = GetWorld()->SpawnActor<APlacableItem>(APlacableItem::StaticClass(), FTransform(ItemRotation, OutHit.ImpactPoint));
		ToPlaceItemRef->SetItemClass(ToPlaceItemClass);
	}
}

void AFirstPersonSurvivalCharacter::OnRotateItemLeft()
{
	ItemRotation.Yaw += 45;
}

void AFirstPersonSurvivalCharacter::OnRotateItemRight()
{
	ItemRotation.Yaw -= 45;
}

bool AFirstPersonSurvivalCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFirstPersonSurvivalCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AFirstPersonSurvivalCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AFirstPersonSurvivalCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}
