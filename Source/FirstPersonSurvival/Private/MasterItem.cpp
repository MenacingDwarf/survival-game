// Fill out your copyright notice in the Description page of Project Settings.


#include "..\Public\MasterItem.h"

// Sets default values
AMasterItem::AMasterItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMasterItem::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMasterItem::ItemSkill(AFirstPersonSurvivalCharacter * PlayerRef)
{
}

// Called every frame
void AMasterItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AMasterItem::UseItem(AFirstPersonSurvivalCharacter * PlayerRef)
{
	if (ItemStructure.bUsable) {
		ItemSkill(PlayerRef);
		Destroy();
		return true;
	}
	return false;
}

