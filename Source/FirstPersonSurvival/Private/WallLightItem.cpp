// Fill out your copyright notice in the Description page of Project Settings.


#include "../Public/WallLightItem.h"

AWallLightItem::AWallLightItem()
{
	ItemStructure.bPlacable = true;
	ItemStructure.bStackable = true;
	ItemStructure.bUsable = false;
	ItemStructure.Description = FText::FromString("Cool wall light for the comfortable atmosphere");
	ItemStructure.Type = EItemType::Thing;
}
