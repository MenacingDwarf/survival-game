// Fill out your copyright notice in the Description page of Project Settings.


#include "../Public/HealthComponent.h"
#include "../FirstPersonSurvivalCharacter.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "../UI/MainHUD.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	bIsDead = false;

	MaxHP = 100.f;
	CurrentHP = MaxHP;

	MaxHunger = 100.f;
	CurrentHunger = MaxHunger;
	PeriodicalHungerLosingAmount = 0.2f;
	PeriodicalHungerLosingFrequency = 2.f;
}


void UHealthComponent::ReceiveDamage(float Damage)
{
	if (!bIsDead) {
		CurrentHP -= Damage;
		ReceivingDamageEffects(Damage);
		if (CurrentHP <= 0) {
			CurrentHP = 0;
			DeathEvent();
		}
		UpdateHealthWidget();
	}
}

void UHealthComponent::ReceiveHeal(float HealSize)
{
	if (!bIsDead) {
		CurrentHP = FMath::Clamp(CurrentHP + HealSize, 0.f, MaxHP);
		UpdateHealthWidget();
		ReceivingHealEffects(HealSize);
	}
}

void UHealthComponent::PeriodicalHungerLosingEvent()
{
	if (CurrentHunger > 0) {
		CurrentHunger = int(FMath::Clamp(CurrentHunger - PeriodicalHungerLosingAmount, 0.f, MaxHunger) * 100) / 100.f;
	}
	else {
		ReceiveDamage(PeriodicalHungerLosingAmount);
	}

	UpdateHealthWidget();

	GetWorld()->GetTimerManager().SetTimer(PeriodicalHungerLosingTimer, this,
										   &UHealthComponent::PeriodicalHungerLosingEvent,
										   PeriodicalHungerLosingFrequency, false);
}

void UHealthComponent::UpdateHealthWidget()
{
	if (PlayerRef->MainHUDRef)
	{
		PlayerRef->MainHUDRef->UpdateHealthSystem(CurrentHP, MaxHP, CurrentHunger, MaxHunger);
	}
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(HealthWidgetInitializingTimer, this,
										   &UHealthComponent::UpdateHealthWidget,
										   0.1, false);

	GetWorld()->GetTimerManager().SetTimer(PeriodicalHungerLosingTimer, this, 
										   &UHealthComponent::PeriodicalHungerLosingEvent, 
										   PeriodicalHungerLosingFrequency, false);
	
}

void UHealthComponent::DeathEvent()
{
	bIsDead = true;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "You are dead, dude!");
	DeathEffects();
}

void UHealthComponent::ReceivingDamageEffects(float Damage)
{
}

void UHealthComponent::ReceivingHealEffects(float HealSize)
{
}

void UHealthComponent::DeathEffects()
{
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

