// Fill out your copyright notice in the Description page of Project Settings.


#include "../Public/PlacableItem.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
APlacableItem::APlacableItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlacableItem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlacableItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlacableItem::SetItemClass(TSubclassOf<class AMasterItem> NewItemClass)
{
	ItemClass = NewItemClass;
	if (ItemClass)
	{
		StaticMesh->SetStaticMesh(GetDefault<AMasterItem>(ItemClass)->ItemStructure.StaticMesh);
	}
}

