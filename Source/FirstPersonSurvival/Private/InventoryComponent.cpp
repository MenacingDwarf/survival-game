// Fill out your copyright notice in the Description page of Project Settings.


#include "../Public/InventoryComponent.h"
#include "../FirstPersonSurvivalCharacter.h"
#include "../Public/MasterItem.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	PlayerRef = Cast<AFirstPersonSurvivalCharacter>(GetOwner());
	SlotsAmount = 20;

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	Slots.SetNum(SlotsAmount);
	// ...
	
}

bool UInventoryComponent::SearchEmptySlot(int & SlotIndex)
{
	for (int i = 0; i < SlotsAmount; i++) {
		if (Slots[i].Amount == 0) {
			SlotIndex = i;
			return true;
		}
	}
	return false;
}

bool UInventoryComponent::SearchSlotForStack(TSubclassOf<AMasterItem> ItemClass, int Amount, int & SlotIndex)
{
	for (int i = 0; i < SlotsAmount; i++) {
		int MaxAmount = ItemClass.GetDefaultObject()->ItemStructure.MaxAmount;
		if (Slots[i].ItemClass == ItemClass && (Slots[i].Amount + Amount) <= MaxAmount) {
			SlotIndex = i;
			return true;
		}
	}
	return false;
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryComponent::AddItem(TSubclassOf<AMasterItem> ItemClass, int Amount)
{
	int SlotIndex = 0;

	// if stackable try to search slot for stack
	if (ItemClass.GetDefaultObject()->ItemStructure.bStackable && SearchSlotForStack(ItemClass, Amount, SlotIndex)) {
		Slots[SlotIndex].Amount += Amount;
		return true;
	}
	
	// if not stackable or can't find slot for stack try to find empty slot
	if (SearchEmptySlot(SlotIndex)) {
		Slots[SlotIndex].ItemClass = ItemClass;
		Slots[SlotIndex].Amount = Amount;
		return true;
	};

	return false;
}

bool UInventoryComponent::UseItemAtIndex(float ItemIndex)
{
	// spawn actor of slot class
	FSlotStructure Slot = Slots[ItemIndex];
	AMasterItem* NewItem = GetWorld()->SpawnActor<AMasterItem>(Slot.ItemClass, FTransform(PlayerRef->GetActorLocation()));

	// try to use this item and remove from inventory
	if (NewItem->UseItem(PlayerRef)) {
		RemoveItemAtIndex(ItemIndex);
		return true;
	}

	return false;
}

bool UInventoryComponent::RemoveItemAtIndex(float ItemIndex, int Amount)
{
	FSlotStructure Slot = Slots[ItemIndex];
	if (Slot.Amount == 0) {
		return false;
	}
	
	if (Slot.Amount <= Amount) {
		Slots[ItemIndex] = FSlotStructure();
	}
	else {
		Slots[ItemIndex].Amount -= Amount;
	}
	return true;
}

